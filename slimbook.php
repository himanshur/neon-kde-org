<?php
header('Location: https://kde.slimbook.es/');
exit;
?>

<?php
/**
 * Available variables for the header:
 * $headerCssFiles = [];
 * $headerJsFiles = [];
 * $headerBodyClasses = '';
 */
	//define('OVERLAY', true);
	$headerCssFiles = ['content/slimbook/slimbook.css'];
	$headerBodyClasses = '';
	include('templates/header.php');
?>


	<main class="KLayout">
		<section id="slimbookHero" class="heroDisplay overlay">
			<article>
				<h1>KDE Slimbook</h1>
				<p>
					Get KDE neon pre-loaded on a sleek and elegant device.
				</p>
				<a href="http://kde.slimbook.es/" target="_blank">Learn More</a>
			</article>
		</section>

		<!-- section>
			<article class="multicolumn-2">
				<div>
					<img src="content/slimbook/cpu-i5.png" class="splashImage" />
					<h3>KDE Intel i5 Model</h3>
					<p style="text-align: center;">
						Featuring the latest officially released KDE software on a stable
						base. Ideal for everyday users.
					</p>
					<a href="https://slimbook.es/en/store/slimbook-kde/kde-intel-i5-comprar" class="buyButton" target="_blank">Shop on the Slimbook Website</a>
				</div>
				<div>
					<img src="content/slimbook/cpu-i7.png" class="splashImage" />
					<h3>KDE Intel i7 Model</h3>
					<p style="text-align: center;">
						Featuring pre-release KDE software built
						the same day from new feature branches.
					</p>
					<a href="https://slimbook.es/en/store/slimbook-kde/kde-intel-i7-comprar" class="buyButton" target="_blank">Shop on the Slimbook Website</a>
				</div>
			</article>
		</section -->
				<style>
					#quotes i+q {
						margin-top: 20px;
					}
					#quotes q {
						display: block;
					}
				</style>
		<section>
				<article>
					<h1>More than the sum of its parts.</h1>
					<p>
						If you enjoy the latest KDE software and want a machine tailored to run the
						Plasma Desktop, this 13'3 inch KDE-branded aluminium travel companion is for you.
						Available with a range of options for memory, CPU and fast SSDs you can customise
						this machine as much as Plasma Desktop.
					</p>
				</article>
		</section>
		<section id="quotes">
			<article>
				<div style="margin: 0px auto; max-width: 500px; text-align: center;">
					<q>
						Something the Linux community has needed for some time.
					</q>
					<i>Tech Republic</i>
					<q>
						This is no hum-drum, clunky chunky of plastic.
					</q>
					<i>OMG Ubuntu!</i>
				</div>
			</article>
		</section>
	</main>

<?php

	include ('templates/footer.php');
?>

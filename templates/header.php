<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>KDE neon</title>
	<!-- IE up to 9, excluding 10, ignoring this rubbish. -->
	<!--[if IE]><link href="media/favicon.ico" rel="shortcut icon" /><![endif]-->
	<!-- IE11/Chrome/Opera/Safari -->
	<link href="media/192x192.png" rel="icon" sizes="192x192" />
	<!-- iOS -->
	<link href="media/180x180.png" rel="apple-touch-icon" sizes="180x180" />
	<!-- Sane Browsers -->
	<link href="media/128x128.svg" rel="icon" sizes="128x128" />
	<link href="media/64x64.svg" rel="icon" sizes="64x64" />
	<link href="media/32x32.svg" rel="icon" sizes="32x32" />
	<link href="media/24x24.svg" rel="icon" sizes="24x24" />
	<link href="media/16x16.svg" rel="icon" sizes="16x16" />
	<!-- JQuery -->
	<script src="media/jquery-2.1.4.min.js"></script>
	<script src="https://www.kde.org/js/tether.min.js" defer></script>
	<script src="https://www.kde.org/js/bootstrap-4.0.0.min.js" defer></script>
	<!-- KGlobalHeader files -->
	<script src="cdn/kGlobalAssets.js?include=header,footer"></script>
	<link href="https://www.kde.org/css/kde-top-bar.css" rel="stylesheet" type="text/css" />
	<link href="cdn/kGlobalAssets.css?include=header,subHeader,footer,layout,social" rel="stylesheet" type="text/css" />
	<link href="page3.css" rel="stylesheet" type="text/css" />
	<link href="https://www.kde.org/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<?php
		if (isset($headerCssFiles))
			foreach($headerCssFiles as $file)
				echo "\n\t".'<link href="'.$file.'" rel="stylesheet" type="text/css" />';

		if (isset($headerScriptFiles))
			foreach($headerScriptFiles as $file)
				echo "\n\t".'<script src="'.$file.'"></script>';
	?>
</head>
<body class="KStandardSiteStyle">
	<header id="KGlobalHeader" class="KNeonHeader">
		<nav id="kHeaderNav" class="navbar navbar-toggleable-md container-fluid" >
			<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<a class="navbar-brand" href="https://www.kde.org/" id="KGlobalLogo">KDE</a>
			<div class="collapse navbar-collapse" id="navbarsExampleDefault">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item">
						<a class="nav-link" href="https://www.kde.org/products/">Products</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="https://www.kde.org/develop">Develop</a>
					</li>
					<li class="nav-item dropdown">
							<a class="nav-link" href="https://community.kde.org/Get_Involved">Get Involved</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="https://www.kde.org/donations">Donate</a>
					</li>
				</ul>
			</div>
		</nav>
		<section id="KSiteNavigation" class="<?php if(defined('OVERLAY')) echo 'overlay'; ?>">
			<div>
				<h1><a href="index"><img src="content/neon-logo.svg" />KDE neon</a></h1>
				<nav>
					<a href="faq">FAQ</a>
					<a href="develop">Developer Edition</a>
					<a href="slimbook" class="button">KDE Slimbook</a>
					<a href="download" class="button">Download KDE neon</a>
				</nav>
			</div>
		</section>
	</header>

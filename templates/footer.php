
	<footer id="KGlobalFooter">
		<section id="KSiteDonateForm">
			<h3>Donate to KDE <a href="https://www.kde.org/community/donations/index.php#money" target="_blank"> Why Donate?</a></h3>
			
			<form 
				action="https://www.paypal.com/en_US/cgi-bin/webscr" 
				method="post" 
				onsubmit="return amount.value >= 2 || window.confirm('Your donation is smaller than 2€. This means that most of your donation\nwill end up in processing fees. Do you want to continue?');">
					<input type="hidden" name="cmd" value="_donations" />
					<input type="hidden" name="lc" value="GB" />
					<input type="hidden" name="item_name" value="Development and communication of KDE software" />
					<input type="hidden" name="custom" value="//neon.kde.org//neon_donation_footer_<?php echo basename($_SERVER['PHP_SELF'])?>" />
					<input type="hidden" name="currency_code" value="EUR" />
					<input type="hidden" name="cbt" value="Return to www.kde.org" />
					<input type="hidden" name="return" value="https://www.kde.org/community/donations/thanks_paypal.php" />
					<input type="hidden" name="notify_url" value="https://www.kde.org/community/donations/notify.php" />
					<input type="hidden" name="business" value="kde-ev-paypal@kde.org" />
					<input type='text' name="amount" value="10.00" id="donateAmountField" /> €
					<button type='submit' id="donateSubmit">Donate via PayPal</button>
			</form>
			
			<a href="https://www.kde.org/community/donations/others.php" id="otherWaysDonate" target="_blank">Other ways to donate</a>
		</section>
		
		<section id="KSiteFooterLinks">
			<nav>
				<h1>Community</h1>
				<a href="https://forum.kde.org/" target="_blank">Community Forums</a><br />
			</nav><nav>
				<h1>Press</h1>
				<a href="https://dot.kde.org/" target="_blank">KDE Dot News</a>
				<a href="https://planet.kde.org" target="_blank">Planet KDE</a>
			</nav><nav>
				<h1>Social</h1>
				<a href="https://twitter.com/hashtag/kde" target="_blank">#kde on Twitter</a>
			</nav>
		</section>

		<section id="KGlobalContributorLinks">
			<nav style="position: relative;">
				<a href="https://community.kde.org/Get_Involved" target="_blank">Get Involved in KDE</a>

				<aside class="kSocialLinks" style="position: absolute; right: 0px; top: 0px;">
                    <a class="shareFacebook" href="https://www.facebook.com/kde/" rel="nofollow">Post on Facebook</a>
                    <a class="shareTwitter" href="https://twitter.com/kdecommunity" rel="nofollow">Share on Twitter</a>
                    <a class="shareDiaspora" href="https://joindiaspora.com/people/9c3d1a454919ef06" rel="nofollow">Share on Diaspora</a>
                    <a class="shareReddit" href="http://www.reddit.com/r/kde/" rel="nofollow">Share on Reddit</a>
                    <a class="shareYouTube" href="https://www.youtube.com/channel/UCF3I1gf7GcbmAb0mR6vxkZQ" rel="nofollow">Share on YouTube</a>
                    <a class="shareMastodon" href="https://mastodon.technology/@kde" rel="nofollow">Share on Mastodon</a>
                    <a class="shareLinkedIn" href="https://www.linkedin.com/company/29561/" rel="nofollow">Share on LinkedIn</a>
				</aside>
			</nav>
		</section>

		<section id="KGlobalLegalInfo">
			<small>
				KDE<sup>&#174;</sup> and the K Desktop Environment<sup>&#174;</sup> logo
				(<figure style="font-family: glyph">K</figure>)</a> are registered trademarks
				of <a href="https://ev.kde.org/" title="Homepage of the KDE non-profit Organization">KDE e.V.</a>
			</small>
			<small>
				<a href="https://www.kde.org/community/whatiskde/impressum.php">Legal</a>
			</small>
		</section>
	</footer>

</body>
</html>

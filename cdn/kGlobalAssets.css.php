<?php
/**
 * Copyright 2016  Kenneth Vermette <vermette@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) version 3, or any
 * later version accepted by the membership of KDE e.V. (or its
 * successor approved by the membership of KDE e.V.), which shall
 * act as a proxy defined in Section 6 of version 3 of the license.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */


/*
 * TODO: We should cache these files when we know the final environment.
 */
	require_once('core.php');


/*
 * Note: if a dependency is just a value, it is used to determine priority
 * when being output. Used to avoid conflicts in CSS and ensure execution
 * order in scripts.
 * This will not walk the dependency tree; each unit must specify all
 * dependencies it requires, it should assume dependencies of dependencies
 * will NOT be puled in.
 */
	$dependencies = [
		'core',
		'animation',
		'header' => 'core,animation,glyphs',
		'subHeader' => 'header,core,animation,glyphs',
		'footer' => 'core,animation,glyphs',
		'social' => 'core,glyphs'
	];


/*
 * If we have no include variable, output some instruction.
 */
	if (!isset($_GET['include'])) {
		outputInstructions('cssAssets/', '.css');
		die();
	}
	else {
		$include = resolveDependencies(explode(',',$_GET['include']), $dependencies);
	}


/*
 * Output the file
 */
	header("Content-type: text/css");
	buildOutput ($include, 'cssAssets/', '.css', "\n\n");

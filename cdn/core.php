<?php
/**
 * Copyright 2016  Kenneth Vermette <vermette@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) version 3, or any
 * later version accepted by the membership of KDE e.V. (or its
 * successor approved by the membership of KDE e.V.), which shall
 * act as a proxy defined in Section 6 of version 3 of the license.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

	if (isset($_GET['debug'])) {
		ini_set('display_errors', 1);
		ini_set('display_startup_errors', 1);
		error_reporting(E_ALL);
	}

	define('K_DOCUMENT_ROOT', substr(dirname(__FILE__), strlen($_SERVER['DOCUMENT_ROOT'])));
	define('K_SITE_ROOT', $_SERVER["SERVER_NAME"].K_DOCUMENT_ROOT);

	function getContent ($document) {
		return !file_exists($document) ?
			'No Content' :
			trim(file_get_contents($document));
	}

	function replaceLocalPaths ($content) {
		return str_replace('local://', '//neon.kde.org/cdn/', $content);
	}

	function resolveDependencies ($requested, $dependencies = []) {
		foreach ($requested as &$r)
			$r = trim($r);

		if (empty($dependencies))
			return $requested;

		$sort   = [];
		$sorted = [];

		foreach ($dependencies as $key => $value)
			$sort[] = is_int($key) ? $value : $key;

		$required = explode(',', implode(',', array_intersect_key($dependencies, array_flip($requested))));
		$required = array_unique(array_merge($required, $requested));

		foreach($sort as $key) {
			if (isset($required[$key])) {
				$sorted[$key] = $required[$key];
				unset($required[$key]);
			}
		}

		return $sorted + $required;
	}


/**
 * Note that this function replaces the shorthand local:// to the root folder
 * of the content delivery site.
 */
	function buildOutput ($include, $folder, $extension, $padding = '') {
		foreach ($include as $inc) {
			$file = $folder.basename($inc).$extension;

			if (file_exists($file)) {
				echo replaceLocalPaths(trim(file_get_contents($file))).$padding;
			}
		}
	}

	function outputInstructions ($folder, $extension) {
		echo '/* You select components to include via the GET include variable: ?include=header,footer'."\n";
		echo ' * The following components are available:'."\n";

		foreach(glob($folder.'*'.$extension) as $file)
			echo ' * - '.basename($file, $extension)."\n";

		echo ' * Note: dependencies will be resolved automatically.'."\n";
	}

	function url ($addr = null)
	{
		$args = array_slice(func_get_args(),1);
		$vars = [];

		foreach ($args as $arg)
			if (is_string($arg))
			{
				parse_str($arg, $temparg);
				$vars = array_merge($vars, $temparg);
			}
			else
				$vars = array_merge($vars, (array)$arg);

		if ($addr == null)
			$addr = $_SERVER['REQUEST_URI'];

		$part = parse_url($addr);

		if (isset($part['path']) && strpos($part['path'], K_DOCUMENT_ROOT) === 0)
			$part['path'] = '/';//substr($part['path'], strlen(K_DOCUMENT_ROOT));

		if (!$part)
			return 'about:blank';

		if (!isset($part['scheme']))
			$part['scheme'] = isset($_SERVER['PROTOCOL']) ? $_SERVER['PROTOCOL'].'://' : '//';
		else
			$part['scheme'] = $part['scheme'].'://';

		if (!isset($part['host']))
			$part['host'] = K_SITE_ROOT;

		$part['query'] = [];
		/*
		if (!isset($part['query']))
			$part['query'] = [];
		else if (is_string($part['query']))
			parse_str($part['query'], $part['query']);

			print_r($part);
			*/
		return
			$part['scheme'].
			$part['host'].
			str_replace('//','/',isset($part['path']) ? '/'.trim($part['path'], '/') : '').
			(count($part['query']) + count($vars) > 0 ? '?'.http_build_query(  array_merge($part['query'], $vars)  ) : '').
			(isset($part['fragment']) ? '#'.$part['fragment'] : '');
	}
